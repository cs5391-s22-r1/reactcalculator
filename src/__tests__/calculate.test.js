import React from 'react';
import calculate from "../logic/calculate";


describe('Calculate Tests', ()=>{
    test('Number button tests', () => {
        expect(calculate({
                previous: null,
                current: null,
                operation: null},"4")).toHaveProperty('current', "4");
        expect(calculate({
                previous: null,
                current: 1,
                operation: null},"1")).toHaveProperty('current', "11");
        expect(calculate({
                previous: null,
                current: null,
                operation: "+"},"1")).toHaveProperty('current', "1");
    });

    test('Addition button tests', () => {
        expect(calculate({
                previous: null,
                current: 1,
                operation: null},"+")).toHaveProperty('current', null);

    });

    test('Equal button tests', () => {
        expect(calculate({
                previous: 2,
                current: 2,
                operation: "+"},"=")).toHaveProperty('previous', "4");
        expect(calculate({
                previous: 2,
                current: 21,
                operation: "+"},"=")).toHaveProperty('previous', "23");
    });

    test('Plus-Minus button tests', () => {
        expect(calculate({
                previous: null,
                current: 1,
                operation: null},"+/-")).toHaveProperty('current', "-1");

    });
    
    test('Plus-Minus button tests', () => {
        expect(calculate({
                previous: null,
                current: 4,
                operation: null},"+/-")).toHaveProperty('current', "-4");

    });

    test('Plus-Minus button tests', () => {
        expect(calculate({
                previous: 8,
                current: 2,
                operation: null},"+/-")).toHaveProperty('current', "-2");

    });

    test('Plus-Minus button tests', () => {
        expect(calculate({
                previous: 3,
                current: null,
                operation: null},"+/-")).toHaveProperty('previous', "-3");

    });

    test('Plus-Minus button tests', () => {
        expect(calculate({
                previous: null,
                current: -5,
                operation: null},"+/-")).toHaveProperty('current', "5");

    });


    test('Plus-Minus button tests', () => {
        expect(calculate({
                previous: null,
                current: null,
                operation: null},"+/-")).toHaveProperty('[]', {});

    });

    test('% button tests with input 1', () => {
        expect(calculate({
                previous: null,
                current: 2,
                operation: null },"%")).toHaveProperty('current', "0.02");
    });

    test('% button tests with input 2', () => {
        expect(calculate({
                previous: 4,
                current: 2,
                operation: "+" },"%")).toHaveProperty('current', null);
    });

    test('% button tests with input 3', () => {
        expect(calculate({
                previous: 34,
                current: 20,
                operation: "-" },"%")).toHaveProperty('current', null);
    });
    test('% button tests with input 4', () => {
        expect(calculate({
                previous: null,
                current: 3333,
                operation: null },"%")).toHaveProperty('current', "33.33");

    });

    /* Test cases for Decimal point */
    test('Decimal point button test #1', () => {
        expect(calculate({
                previous: null,
                current: null,
                operation: null},".")).toHaveProperty('current', "0.");
    });
    test('Decimal point button test #2', () => {
        expect(calculate({
                previous: null,
                current: "5",
                operation: null},".")).toHaveProperty('current', "5.");
    });
    test('Decimal point button test #3', () => {
        expect(calculate({
                previous: "3.",
                current: "5",
                operation: "+"},".")).toHaveProperty('current', '5.');
    });
    test('Decimal point button test #4', () => {
        expect(calculate({
                previous: null,
                current: "2.5",
                operation: null},".")).toHaveProperty('[]',{});  
    });
});
