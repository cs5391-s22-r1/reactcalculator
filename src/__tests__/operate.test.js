import React from 'react';
import operate from "../logic/operate";
import Big from "big.js";



describe('Operate Multiplication Tests', ()=>{
    test('4 x 3 = 12 test', () => {
        expect(operate("4","3","x")).toBe("12"); 
    });
    test('16.2 x 6 = 97.2 test', () => {
        expect(operate("16.2","6","x")).toBe("97.2"); 
    });
    test('-16.2 x 6 = -97.2 test', () => {
        expect(operate("-16.2","6","x")).toBe("-97.2"); 
    });
    test('9.63 x 5.21 = 50.1723 test', () => {
        expect(operate("9.63","5.21","x")).toBe("50.1723"); 
    });
    test('1.23 x 3.21 = 3.94 test', () => {
        expect(parseFloat(operate("1.23","3.21","x"))).toBeCloseTo(3.94, 1); 
    });
    test('-9.95 x 2.2 = -21.89 test', () => {
        expect(operate("-9.95","2.2","x")).toBe("-21.89"); 
    });
});


describe('Operate Division Tests', ()=>{
    test('4/3 = 1.333 test', () => {
        expect(parseFloat(operate("4","3","÷"))).
            toBeCloseTo(1.3333333333, 5); 
        // parseFloat() is a string-to-float function in Javascript.   
        // Try different precisions and string lengths to see what the test does.
    });
    test('20/5 = 4 test', () => {
        expect(operate("20","5","÷")).toBe("4"); 
    });
    test('-3/2 = -1.5 test', () => {
        expect(operate("-3","2","÷")).toBe("-1.5"); 
    }); 
    test('45.6/6.58 = 6.93 test', () => {
        expect(parseFloat(operate("45.6","6.58","÷"))).toBeCloseTo(6.93, 2); 
    });
    test('1.23/3.21 = 0.383177 test', () => {
        expect(parseFloat(operate("1.23","3.21","÷"))).toBeCloseTo(0.3831, 2); 
    });
    test('-9.95/8.7 = 1.143678 test', () => {
        expect(parseFloat(operate("-9.95","8.7","÷"))).toBeCloseTo(-1.1436, 2); 
    });
});


describe('Operate Addition Tests', ()=>{
    test('(-4) + 3 = -1 test', () => {
        expect(operate("-4","3","+")).toBe("-1"); 
    });
    test('10 + (-16) = -6 test', () => {
        expect(operate("10","-16","+")).toBe("-6"); 
    });
    test('-3.3 + -5.5 = -8.8 test', () => {
        expect(operate("-3.3","-5.5","+")).toBe("-8.8"); 
    });
    test('-9.3 + 5.5 = -3.8 test', () => {
        expect(operate("-9.3","5.5","+")).toBe("-3.8"); 
    });
});


describe('Operate Subtraction Tests', ()=>{
    test('4 - 7 = -3 test', () => {
        expect(operate("4","7","-")).toBe("-3"); 
    });
    test('-10 - 6 = -16 test', () => {
        expect(operate("-10","6","-")).toBe("-16"); 
    });
    test('3 - 4.2 = -1.2 test', () => {
        expect(operate("3","4.2","-")).toBe("-1.2"); 
    });
    test('-1.6 - 4.2 = -5.8 test', () => {
        expect(operate("-1.6","4.2","-")).toBe("-5.8"); 
    });
});



